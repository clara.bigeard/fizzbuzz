#include "fizzbuzz.h"

string fizzbuzz(int val){
    if (val%3 == 0 and val%5 == 0)
    {
        return "FizzBuzz";
    }
    else if (val%3 == 0)
    {
        return "Fizz";
    }
    else if (val%5 == 0)
    {
        return "Buzz";
    }
    return to_string(val);
}