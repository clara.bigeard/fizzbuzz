#include "fizzbuzz.h"
#include "gtest/gtest.h"
//https://google.github.io/googletest/reference/assertions.html

TEST(FizzTest, ValidNumbers) 
{
    
    for (int j = 3; j < 1000; j = j*3)
    {
        if (j%5 != 0)
        {
            ASSERT_EQ("Fizz",fizzbuzz(j));
        }
    }
}

TEST(BuzzTest, ValidNumbers) 
{
    for (int j = 5; j < 1000; j = j*5)
    {
        if (j%3 != 0)
        {
            ASSERT_EQ("Buzz",fizzbuzz(j));
        }
    }
}

TEST(FizzBuzzTest, ValidNumbers) 
{
    for (int j = 3; j < 1000; j++)
    {
        if (j%3 == 0 and j%5 == 0)
        {
            ASSERT_EQ("FizzBuzz",fizzbuzz(j));
        }
    }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
