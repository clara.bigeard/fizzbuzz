# FizzBuzz

## Règles
On compte de 1 à 1000.  
A chaque nombre on chercher:  
* Si le nombre est un multiple de 3 alors on doit dire "Fizz" à la place
* Si le nombre est un multiple de 5 alors on doit dire "Buzz" à la place
* Si le nombre est un multiple de 3 et de 5 alors on doit dire "FizzBuzz" à la place
